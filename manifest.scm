;;; Defines jobs and run order for use with tomd.

(add-to-load-path "/home/niebie/sc/tomd/guile")

(define-module (tomd manifest)
  #:use-module (tomd job)
  #:export (job-list))

;;; job details:
;;;  command-line
;;;  args
;;;  start-trigger (boot, shutdown, hourly, login, logout)
;;;  end-trigger   (boot, shutdown, hourly, login, logout)

(define set-bg-job
  (create-job #:command-line "xsetroot"
              #:args (list "-solid"
                           "grey")
              #:start-trigger 'login))

(define start-fetchmail-job
  (create-job #:command-line "fetchmail"
              #:args (list "--ssl"
                           "--verbose")
              #:start-trigger 'login
              #:end-trigger 'logout))

(define start-emacs-daemon-job
  (create-job #:command-line "emacs"
              #:args (list "--daemon")
              #:start-trigger 'login
              #:end-trigger 'logout))

(define start-spoon-job
  (create-job #:command-line "/home/niebie/sc/spoon-fork/spoon"
              #:start-trigger 'login
              #:end-trigger 'logout
              #:redirect #t
              #:name "spoon"))

(define job-list
  (list set-bg-job
        ;; start-fetchmail-job
        ;; start-emacs-daemon-job
        start-spoon-job
        ))
