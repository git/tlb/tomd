/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static struct{
  char *buf;
}options;

struct line{
  char *buf;
  struct line *next;
};

static struct line *find_lines(void)
{
  struct line *lines = malloc(sizeof(struct line));
  struct line *cur_line = lines;
  struct line *old_line = lines;
  char *ptr = options.buf;
  lines->buf = options.buf;
  lines->next = NULL;
  while(1){
    if(*ptr == '\n'){
      /* printf("----\n"); */
      /* printf("found a new line.\n"); */
      /* printf("'%c' -> \\0\n", *ptr); */
      /* printf("'next \%c': %c\n", *(ptr+1)); */
      *ptr = '\0';
      ptr++;
      if(*ptr == '\0'){
        /* end of buf */
        /* printf("found the end. (1)\n"); */
        return lines;
      }

      while(*ptr == '\n'){
        /* empty, discard */
        /* printf("empty\n"); */
        *ptr = '\0';
        ptr++;
      }

      if(*ptr == '\0'){
        /* end of buf */
        /* printf("found the end. (2)\n"); */
        return lines;
      }

      /* printf("hallo there.\n"); */
      cur_line = malloc(sizeof(struct line));
      old_line->next = cur_line;
      old_line = cur_line;
      cur_line->buf = ptr;
    }
    /* printf("|%c|\n", *ptr);  */
    ptr++;
  }      
}

int main(int argc, char **argv)
{
  options.buf = malloc(sizeof(char) * 1024);
  strcpy(options.buf,
         "THIS IS A TEST BUFFER.\nWHAT A LINE (1)\n"
         "AND ANOTHER ONE WHAT A FUCKING WORLD.\n\n\n\n\n\n\n\n\n"
         "TEST THE LAST\n\n"
         "JUST KIDDING HERE IS THE LAST LINE\n\0"
         "WHAT ABOUT THIS ONE YOU FREAK OF NATURE\0\0\0");

  struct line *result = find_lines();
  struct line *cur_line = result;

  int count = 0;
  while(1){
    printf("[%d] '%s'\n", count++, cur_line->buf);
    if(cur_line->next == NULL){
      /* printf("found the end.\n"); */
      break;
    }
    cur_line = cur_line->next;
  }
}
