/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

/* Commentary: */
/* This file contains functions to be used by both client and server */
/* communicate between the two */

static int sfd;

int send_to_server(char *buffer, size_t len)
{
  if(buffer == NULL){
    printf("buffer cannot be null\n");
    return -1;
  }

  if(len == 0){
    return 0;
  }

  if(sfd < 0){
    printf("server connection lost.\n");
    return -1;
  }

}
