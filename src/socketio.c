/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>

#define MAX_LEN 1024

struct line{
  char *buf;
  struct line *next;
};

static struct{
  char *buf;
  int sfd;
  struct line *lines;
}options;

void setup_socket(int sfd)
{
  options.sfd = sfd;
  options.buf = malloc(sizeof(char) * MAX_LEN);
}

static struct line *find_lines(int size)
{
  /* printf("entering find_lines\n"); */
  struct line *lines = malloc(sizeof(struct line));
  struct line *cur_line = lines;
  struct line *old_line = lines;
  char *ptr = options.buf;
  lines->buf = options.buf;
  lines->next = NULL;
  int c = 0;
  /* printf("starting loop.\n"); */
  while(1){
    if(c >= size){
      /* printf("killing read at end of reported read.\n"); */
      /* printf("(3) used [%d] of [%d]\n", c, size); */
      /* maybe put a null at end of cur_line->buf */
      return lines;
    }
    if(*ptr == '\n' || *ptr == '\0'){
      /* printf("----\n"); */
      /* printf("found a new line.\n"); */
      /* printf("'%c' -> \\0\n", *ptr); */
      /* printf("'next \%c': %c\n", *(ptr+1)); */
      /* if(*ptr == '\0'){ */
      /*   /\* end of buf *\/ */
      /*   /\* printf("found the end. (1)\n"); *\/ */
      /*   printf("(1) used [%d] of [%d]\n", c, size); */
      /*   return lines; */
      /* } */

      *ptr = '\0';
      ptr++; c++;

      while(*ptr == '\n' || *ptr == '\0'){
        if(c >= size){
          /* printf("killing read at end of reported read.\n"); */
          /* printf("(3) used [%d] of [%d]\n", c, size); */
          return lines;
        }
        /* empty, discard */
        /* printf("empty\n"); */
        *ptr = '\0';
        ptr++; c++;
      }

      /* if(*ptr == '\0'){ */
      /*   /\* end of buf *\/ */
      /*   /\* printf("found the end. (2)\n"); *\/ */
      /*   printf("(2) used [%d] of [%d]\n", c, size); */
      /*   return lines; */
      /* } */

      /* printf("hallo there.\n"); */
      cur_line = malloc(sizeof(struct line));
      old_line->next = cur_line;
      old_line = cur_line;
      cur_line->buf = ptr;
      cur_line->next = NULL;
    }
    /* printf("________|%c|\n", *ptr); */
    ptr++; c++;
  }
}

void socket_read(void (*func)(char *line))
{
  if(options.lines != NULL){
    /* printf("|||| already got some input. |||\n"); */
    goto call_func;
  }

  int size;
 do_read:
  /* printf("||||| doing read. ||||\n"); */
  memset(options.buf, 0, MAX_LEN);              \
  size = read(options.sfd, options.buf, MAX_LEN);
  /* printf("read size: %d\n", size); */
  /* printf("||||| got read. ||||\n"); */
  if(size == 0){
    /* printf("||||| reading again. ||||\n"); */
    goto do_read;
  }else{
    /* printf("||||| done read. ||||\n"); */
    options.lines = find_lines(size);

    /* struct line *current_line = options.lines; */

    /* int count = 0; */
    /* while(1){ */
    /*   printf("[%d] '%s'\n", count++, current_line->buf); */
    /*   if(current_line->next == NULL){ */
    /*     printf("found the end.\n"); */
    /*     break; */
    /*   } */
    /*   current_line = current_line->next; */
    /* } */
  }

 call_func:{
    struct line *cur_line = options.lines;
    if(cur_line == NULL){
      /* printf("bad options line.\n"); */
    }else{
      while(1){
        /* printf("[%s]->", cur_line->buf); */
        if(cur_line->next == NULL){
          /* printf("(nil)\n"); */
          break;
        }else{
          cur_line = cur_line->next;
        }
      }
    }
  }

  /* printf("calling function with %s\n", options.lines->buf); */
  func(options.lines->buf);
  options.lines = options.lines->next;
  return;
}
