/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#include <unistd.h>

struct job{
  char *name;
  char *cmd;
  char *args[10];
  pid_t pid;
  int last_status;
  int redirect;
  int root;
  /* todo */
  /* enum trigger start_trigger; */
  /* enum trigger end_trigger; */
};
