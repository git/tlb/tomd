#ifndef _MACROS_H
#define _MACROS_H

#include <errno.h>

#define tomd_panic(...) {                                               \
    printf("[tomd]PANIC ");                                             \
    printf(__VA_ARGS__);                                                \
    printf(" errno=%d\n", errno);                                       \
    for(;;){}                                                           \
  }
#define tomc_p(...) {printf("[tomc]      "); printf(__VA_ARGS__); printf("\n");}
#define tomd_p(...) {printf("[tomd]      "); printf(__VA_ARGS__); printf("\n");}
#define SCM_ARR(arr, index) (scm_list_ref(arr, scm_from_int(index)))
#define SCM_LIST_LEN(list) (scm_to_int(scm_length(list)))
#define WRAP_SCM_FUNCTION_1(module, scm_name, name)    \
SCM name(SCM obj) {                                  \
  static SCM func_ref = SCM_UNDEFINED;               \
  if(func_ref == SCM_UNDEFINED){                     \
    func_ref = scm_c_public_ref(module, scm_name);   \
  }                                                  \
  return scm_call(func_ref, obj, SCM_UNDEFINED);     \
}

#endif
