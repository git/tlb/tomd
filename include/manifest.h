/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

/* SUMMARY: */
/* Load jobs from config file into tomd main memory. */
void load_jobs(void);

/* SUMMARY: */
/* Run each job in sequence via fork + execv */
void run_jobs(void);

/* SUMMARY: */
/* Lookup in the job table the job with a given name */
struct job *lookup_job(char *buf);

void silent(void);
void check_root_job(void);

/* for tomc */
void setup_socket(int sfd);
void socket_read(void (*func)(char *line));
