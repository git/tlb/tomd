/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

/* Commentary: */
/* This header contains functions and data structures intended for */
/* use with the tomd daemon. These constitute the standard interface */
/* for IPC with the daemon, giving commands and allowing for progress */
/* polling and statuses. */

enum tomd_code{
  daemon_start = 0,
  daemon_stop,
  daemon_restart,
  daemon_status,
  update_title_field,
  music_track_current,
  music_track_skip,
  music_vol_up,
  music_vol_down,
  music_vol_mute,
  network_status,
  run_script
};

struct tomd_message{
  enum tomd_code code;
  void *param;
};
